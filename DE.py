class DE:  # Deutsch
    def __init__(self):
        self.text = ''
        self.color_text = ''
        self.words = []
        self.A_Words = []

    def set_text(self, text):
        self.text = text
        self.words = text.split()

    def set_color_text(self, text):
        self.color_text = text

    def get_text(self):
        return self.text

    def get_color_text(self):
        return self.color_text

    class Pronoun:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.Amount = ''
            self.Voice = ''
            self.Person = ''

            self.Part_Speech = 'Pronoun'
            self.Part_Sentence = ''

            self.Dict_Index = 0

    class Noun:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix1 = ''

            self.state = ''
            self.Amount = ''
            self.Voice = ''

            self.Part_Speech = 'Noun'
            self.Part_Sentence = ''

            self.Dict_Index = 0

    class Verb:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.Time = ''
            self.complexity = ''
            self.Modal_Verb = ''

            self.Part_Speech = 'Verb'
            self.Part_Sentence = 'Predicate'

            self.Dict_Index = 0
            self.Dict_Index_Modal_Verb = 0

    class Preposition:
        def __init(self):
            self.Word = ''

            self.Part_Speech = 'Preposition'
            self.Part_Sentence = 'Preposition'

            self.Dict_Index = 0

